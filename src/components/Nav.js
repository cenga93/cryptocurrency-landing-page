const Nav = () => {
     return (
          <nav className='flex items-center'>
               <ul className='flex gap-x-8'>
                    <li className='border-b-2 border-transparent hover:border-blue transition-all duration-300'>
                         <a href='/#'>Producers</a>
                    </li>
                    <li className='border-b-2 border-transparent hover:border-blue transition-all duration-300'>
                         <a href='/#'>Features</a>
                    </li>
                    <li className='border-b-2 border-transparent hover:border-blue transition-all duration-300'>
                         <a href='/#'>About</a>
                    </li>
                    <li className='border-b-2 border-transparent hover:border-blue transition-all duration-300'>
                         <a href='/#'>Contact</a>
                    </li>
               </ul>
          </nav>
     );
};

export default Nav;
