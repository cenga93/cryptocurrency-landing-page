import { CgClose } from 'react-icons/cg';

const NavMobile = ({ setMobileNav }) => {
     return (
          <nav className='lg:hidden bg-violet h-full top-0 bottom-0 w-80 flex justify-center items-center'>
               <div onClick={() => setMobileNav(false)} className='absolute top-8 right-6 cursor-pointer'>
                    <CgClose className='text-3xl' />
               </div>
               <ul className='text-xl flex-col gap-y-8'>
                    <li>Home</li>
                    <li>About</li>
                    <li>Services</li>
                    <li>Contact</li>
               </ul>
          </nav>
     );
};

export default NavMobile;
