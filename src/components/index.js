import Hero from './Hero';
import Header from './Header';
import Nav from './Nav';
import AccountButtons from './AccountButtons';
import Stats from './Stats';
import NavMobile from './NavMobile';
import Why from './Why';
import Calculate from './Calculate';
import CalcForm from './CalcForm';
import Trade from './Trade';
import Features from './Features';
import Newsletter from './Newsletter';
import Footer from './Footer';

export { Hero, Header, Nav, AccountButtons, Stats, NavMobile, Why, Calculate, CalcForm, Trade, Features, Newsletter, Footer };
