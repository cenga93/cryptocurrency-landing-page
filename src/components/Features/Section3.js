import Image from '../../assets/images/feature-3-img.png';

const Section3 = () => {
     return (
          <section className='py-[30px] lg:py-[120px] pt-0'>
               <div className='flex flex-col lg:flex-row'>
                    <div className='max-w-[450px] mb-6 lg-mt-10' data-aos='fade-right' data-aos-offset='450'>
                         <h3 className='h3 mb-6'>Grow your profit and track your investments</h3>
                         <p className='text-gray mb-8 max-w-[408px]'>
                              Use advanced analytics tools. Clear TradingView charts let you track current and historical profit
                              investments.
                         </p>
                         <button className='btn px-8'>Learn more</button>
                    </div>
                    <div className='flex flex-1 justify-end' data-aos='fade-left' data-aos-offset='450'>
                         <img src={Image} alt='' />
                    </div>
               </div>
          </section>
     );
};

export default Section3;
