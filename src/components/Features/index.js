import Section1 from './Section1';
import Section2 from './Section2';
import Section3 from './Section3';

const Features = () => {
     return (
          <section className='section'>
               <div className='container mx-auto'>
                    <div className='text-center max-w-[758px] mx-auto mb-24'>
                         <h2 className='section-title' data-aos='fade-up' data-aos-offset='400'>
                              Market sentiments, portfolio and run the infrastructure of your choice
                         </h2>
                    </div>
                    <Section1 />
                    <Section2 />
                    <Section3 />
               </div>
          </section>
     );
};

export default Features;
