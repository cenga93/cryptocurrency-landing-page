/** components */
import { Hero, Header, NavMobile, Stats, Why, Calculate, Trade, Features, Newsletter, Footer } from './components';
import { useEffect, useState } from 'react';
import Aos from 'aos';

const App = () => {
     const [mobileNav, setMobileNav] = useState(false);

     useEffect(() => {
          Aos.init({
               duration: 2500,
               delay: 400,
          });
     }, []);

     return (
          <div className='overflow-hidden'>
               <Header setMobileNav={setMobileNav} />
               <Hero />
               <div className={`${mobileNav ? 'right-0' : '-right-full'} fixed z-10 top-0 h-full transition-all duration-200`}>
                    <NavMobile setMobileNav={setMobileNav} />
               </div>
               <Stats />
               <Why />
               <Calculate />
               <Trade />
               <Features />
               <Newsletter />
               <Footer />
          </div>
     );
};

export default App;
